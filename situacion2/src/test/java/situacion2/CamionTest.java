package situacion2;

import org.junit.Test;
import static org.junit.Assert.*;

public class CamionTest {


    @Test
    public void precioAlquiler()
    {
        Camion camion = new Camion("Ford", "AB125", 50, 50, false);
        double total;
        total = camion.precioAlquiler();
        assertEquals(500,total,0.00);
    }

    @Test
    public void alquilarVehiculoFalse()
    {
        Camion camion = new Camion("Ford", "AB125", 50, 50, false);
        assertEquals(false, camion.alquilado);
    }

    @Test
    public void alquilarVehiculoTrue()
    {
        Camion camion = new Camion("Ford", "AB125", 50, 50, true);
        assertEquals(true, camion.alquilado);
    }


}
