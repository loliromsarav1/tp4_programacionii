package situacion2;
import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class AutoTest {
    
    @Test
    public void testAlquilarVehiculoFalse() {

        Auto auto = new Auto("Toyota", "AB125", false,5);
        assertEquals(false, auto.alquilado);
    }

    @Test
    public void testAlquilarVehiculoTrue() {

        Auto auto = new Auto("Toyota", "AB125",true,5);
        assertEquals(true, auto.alquilado);
    }

    @Test
    public void testPrecioAlquiler() {
        Auto auto = new Auto("Toyota", "AB125", false,5);
        double total;
        total = auto.precioAlquiler();
        assertEquals(250, total,0.00);

    }
}
