package Excepciones;


public class ListaVehiculoVaciaException extends Exception{

    public ListaVehiculoVaciaException(String mensaje) {
        super(mensaje);
    }
}
