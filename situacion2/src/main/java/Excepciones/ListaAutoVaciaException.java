package Excepciones;


public class ListaAutoVaciaException extends Exception {

    public ListaAutoVaciaException(String mensaje) {
        super(mensaje);
    }
    
}
