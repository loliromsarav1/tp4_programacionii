package Excepciones;

public class ComponenteErroneoException extends Exception{

    public ComponenteErroneoException(String mensaje) {
        super(mensaje);
    }
}
