package situacion2;

public abstract class VehiculoPasajeros extends Vehiculo {

   protected final int PRECIO_POR_PLAZA_Y_DIA=50;
   protected int cantidadPlazas;

   
    public VehiculoPasajeros(String marca, String patente, int precioAlquilerDiario, int cantidadPlazas) {
         super(marca, patente, precioAlquilerDiario);
        this.cantidadPlazas = cantidadPlazas;
    }


    public int getPRECIO_POR_PLAZA_Y_DIA() {
        return PRECIO_POR_PLAZA_Y_DIA;
    }
    
    
    public int getCantidadPlazas() {
        return cantidadPlazas;
    } 
    
}
