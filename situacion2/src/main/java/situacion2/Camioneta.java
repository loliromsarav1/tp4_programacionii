package situacion2;

public class Camioneta extends VehiculoCarga implements InterfaceAlquilerVentaVehiculo{

    public Camioneta(int kilometraje, String marca, String patente, int precioAlquilerDiario,
            float pRECIO_POR_KILOMETRAJE) {
        super(kilometraje, marca, patente, precioAlquilerDiario, pRECIO_POR_KILOMETRAJE);
    }

    @Override
    public float precioAlquiler(int diasAlquilado)
    {
        return (float)((this.getPrecioAlquilerDiario()*diasAlquilado)+(this.getPRECIO_POR_KILOMETRAJE()));
    }

    @Override
    public float precioVenta() {
        // TODO Auto-generated method stub
        return 0;
    }
}
