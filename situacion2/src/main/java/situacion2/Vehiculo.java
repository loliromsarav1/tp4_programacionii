package situacion2;

public abstract class Vehiculo{

    protected String marca;
    protected String patente;
    protected int precioAlquilerDiario;


    public Vehiculo(String marca, String patente, int precioAlquilerDiario) {
        this.marca = marca;
        this.patente = patente;
        this.precioAlquilerDiario = precioAlquilerDiario;
    }


    public String getMarca() {
        return marca;
    }


    public String getPatente() {
        return patente;
    }



    public int getPrecioAlquilerDiario() {
        return precioAlquilerDiario;
    }

    

}