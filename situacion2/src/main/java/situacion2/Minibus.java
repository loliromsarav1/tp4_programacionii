package situacion2;

public class Minibus  extends VehiculoPasajeros implements InterfaceAlquilerVentaVehiculo{

    public final int PRECIO_SEGURO;

    public Minibus(String marca, String patente, int precioAlquilerDiario, int cantidadPlazas, int pRECIO_SEGURO) {
        super(marca, patente, precioAlquilerDiario, cantidadPlazas);
        PRECIO_SEGURO = pRECIO_SEGURO;
    }

    public int getPRECIO_SEGURO() {
        return PRECIO_SEGURO;
    }

    @Override
    public float precioAlquiler(int diasAlquilado)
    {
        return (float)((this.getPrecioAlquilerDiario()*diasAlquilado)+(this.PRECIO_POR_PLAZA_Y_DIA*this.getCantidadPlazas())+(this.getPRECIO_SEGURO()+this.getCantidadPlazas()));
    }

    @Override
    public float precioVenta() {
        // TODO Auto-generated method stub
        return 0;
    }
}
