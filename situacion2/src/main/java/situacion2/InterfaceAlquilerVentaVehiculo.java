package situacion2;

public interface InterfaceAlquilerVentaVehiculo {
    float precioAlquiler(int diasAlquilado);
    float precioVenta();

}
