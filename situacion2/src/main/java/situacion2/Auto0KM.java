package situacion2;

import Excepciones.ComponenteErroneoException;

public class Auto0KM extends Auto{
    private  ExtrasOKM componentes  =  new ExtrasOKM();

    public Auto0KM(String marca, String patente, int precioAlquilerDiario, int cantidadPlazas, int precioBaseVenta) {
        super(marca, patente, precioAlquilerDiario, cantidadPlazas, precioBaseVenta);
    }
    
    
    public void sumarComponente(String componente) throws ComponenteErroneoException
    {
        componentes.agregarComponente(componente);
    }

    public ExtrasOKM getComponentes() {
        return componentes;
    }
    
    @Override
    public float precioVenta()
    {
        float precio;
        precio =  (float)(this.getPrecioBaseVenta()+(this.getPrecioBaseVenta()*0.5));
        for(String componente : this.componentes.getComponentes())
        {
            switch(componente){
                case "Alarma":
                        precio  +=  this.getPrecioBaseVenta()*0.01;
                        break;
                case "Levanta cristales":
                        precio += this.getPrecioBaseVenta()*0.05;
                        break;
                case "Aire  acondicionado":
                        precio += this.getPrecioBaseVenta()*0.02;
                        break;
            }
        }
        return precio;
    }

    @Override
    public float precioAlquiler(int diasAlquilado) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
}
