package situacion2;

public class Camion extends VehiculoCarga implements InterfaceAlquilerVentaVehiculo{

    public final int PRECIO_ABONO = 200;

    public Camion(int kilometraje, String marca, String patente, int precioAlquilerDiario,
            float pRECIO_POR_KILOMETRAJE) {
        super(kilometraje, marca, patente, precioAlquilerDiario, pRECIO_POR_KILOMETRAJE);
    }

    public int getPRECIO_ABONO() {
        return PRECIO_ABONO;
    }

    @Override
    public float precioAlquiler(int diasAlquilado)
    {
        return (float)((this.getPrecioAlquilerDiario()*diasAlquilado)+(this.getPRECIO_ABONO()+this.getPRECIO_POR_KILOMETRAJE()));
    }

    @Override
    public float precioVenta() {
        // TODO Auto-generated method stub
        return 0;
    }
}
