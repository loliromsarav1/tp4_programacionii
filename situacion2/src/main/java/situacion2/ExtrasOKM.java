package situacion2;

import Excepciones.ComponenteErroneoException;
import java.util.ArrayList;
import java.util.List;

public class ExtrasOKM {
    private List<String> componentes = new ArrayList();
    
    public void agregarComponente(String  componente) throws ComponenteErroneoException
    {
        if(componente.equals("Alarma") || componente.equals("Levanta cristales") || componente.equals("Aire acondicionado"))
        {
            componentes.add(componente);
        }
        else
        {
            throw new ComponenteErroneoException ("El componente que se desea agregar es incorrecto o no se encuentra disponible");
        }
    }

    public List<String> getComponentes() {
        return componentes;
    }
    
    
}
