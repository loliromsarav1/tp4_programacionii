package situacion2;

public class AutoUsado extends Auto implements InterfaceAlquilerVentaVehiculo {

    public AutoUsado(String marca, String patente, int precioAlquilerDiario, int cantidadPlazas, int precioBaseVenta) {
        super(marca, patente, precioAlquilerDiario, cantidadPlazas, precioBaseVenta);
    }

    @Override
    public float precioAlquiler(int diasAlquilado)
    {
        return (float)(this.getPrecioAlquilerDiario()*diasAlquilado)+(this.getPRECIO_POR_PLAZA_Y_DIA()*this.getCantidadPlazas());

    }

    @Override
    public float precioVenta()
    {
        return (float)(this.getPrecioBaseVenta()+(this.getPrecioBaseVenta()*0.35));
    }
}
