package situacion2;

import Excepciones.ListaAutoVaciaException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import Excepciones.ListaVehiculoVaciaException;

public class Agencia implements InterfaceAlquilerVentaVehiculo {
    private  String  nombre;
    private List<Vehiculo> vehiculosAlquiler  = new ArrayList<>();
    private List<Auto>  autos = new  ArrayList<>();

    public Agencia(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }
   
    
    public void registrarVehiculo(Vehiculo vehiculo)
    {
        if(InterfaceAlquilerVentaVehiculo.class.isAssignableFrom(vehiculo.getClass()))
        {
            Vehiculo  vehiculoAlquiler  = (Vehiculo) vehiculo;
            vehiculosAlquiler.add(vehiculoAlquiler);
        }
        if(vehiculo instanceof Auto0KM || vehiculo  instanceof AutoUsado)
        {
            Auto auto  = (Auto)  vehiculo;
            registrarAuto(auto);
        }
    }
    
    public  void registrarAuto(Auto  auto)
    {
        autos.add(auto);
    }

    @Override
    public float precioAlquiler(int diasAlquilado) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public float precioVenta() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    public boolean existeVehiculo(String  patente)
    {
        boolean  existe  =  false;
        for(Vehiculo vehiculo : this.vehiculosAlquiler)
        {
            if(vehiculo.getPatente().equals(patente))
            {
                existe  = true;
            }
        }
        return existe;
    }
    
    public boolean existeAuto(String patente)
    {
        boolean existe = false;
        for(Auto auto  : this.autos)
        {
            if(auto.getPatente().equals(patente))
            {
                existe = true;
            }
        }
        
        return existe;
    }
    
    
    public Vehiculo buscarV(String patente)
    {
        Vehiculo encontrado =  null;
        if(existeVehiculo(patente))
        {
            for(Vehiculo vehiculo : this.vehiculosAlquiler)
            {
                if(vehiculo.getPatente().equals(patente))
                {
                    encontrado =  vehiculo;
                }
            }
        }
        return encontrado;
    }
    
    public Auto buscarA(String  patente)
    {
        Auto encontrado  =  null;
        if(existeAuto(patente))
        {
            for(Auto  auto  : this.autos)
            {
                if(auto.getPatente().equals(patente))
                {
                    encontrado = auto;
                }
            }
        }
        return encontrado;
    }
    
    public List<Vehiculo> listarPorPrecioVehiculo() throws ListaVehiculoVaciaException
    {
        if(!this.vehiculosAlquiler.isEmpty())
        {
            Collections.sort(this.vehiculosAlquiler, new Comparator<Vehiculo>()
            {
                @Override
                public int compare(Vehiculo objeto1,  Vehiculo objeto2)
                {
                    return Integer.valueOf(objeto1.getPrecioAlquilerDiario()).compareTo(objeto2.getPrecioAlquilerDiario());
                }
            });
            return this.vehiculosAlquiler;
        }
        else
        {
            throw new ListaVehiculoVaciaException ("De momento no hay disponibilidad de vehiculos para alquilar");
        }
    }
    
    public List<Auto> listarPorPrecioAuto() throws ListaAutoVaciaException
    {
        if(!this.autos.isEmpty())
        {
            Collections.sort(this.autos, new Comparator<Auto>()
            {
                @Override
                public int compare(Auto objeto1, Auto  objeto2)
                {
                    float precioAutoX;
                    if(objeto1 instanceof Auto0KM)
                    {
                        Auto0KM autoOKM = (Auto0KM)objeto1;
                        precioAutoX = autoOKM.precioVenta();
                    }
     
                    else
                    {
                        AutoUsado autoUsado = (AutoUsado)objeto1;
                        precioAutoX = autoUsado.precioVenta();
                    }
                    
                    
                    float precioAutoY;
                    if(objeto2 instanceof Auto0KM)
                    {
                        Auto0KM autoOKM = (Auto0KM)objeto2;
                        precioAutoY = autoOKM.precioVenta();
                    }
                    else
                    {
                        AutoUsado autoUsado = (AutoUsado)objeto2;
                        precioAutoY = autoUsado.precioVenta();
                    }
                    return Float.valueOf(precioAutoX).compareTo(precioAutoY);
                 }
                
             });
            return this.autos;
         }
        else
        {
            throw new ListaAutoVaciaException ("De momento no hay disponibilidad de autos para alquilar.");
        }
    }      
    
    
   public void eliminarRegistroVehiculo(Vehiculo vehiculo)
   {
       if(existeVehiculo(vehiculo.getPatente()))
       {
           Vehiculo eliminado = null;
           for(Vehiculo porEliminar : this.vehiculosAlquiler)
           {
               if(porEliminar.getPatente().equals(vehiculo.getPatente()))
               {
                   eliminado = porEliminar;
               }
           }
           this.vehiculosAlquiler.remove(eliminado);
       }
   }
   
   
   public void eliminarRegistroAuto(Auto auto)
   {
       if(existeAuto(auto.getPatente()))
       {
           Auto eliminado = null;
           for(Auto porEliminar : this.autos)
           {
               if(porEliminar.getPatente().equals(auto.getPatente()))
               {
                   eliminado = porEliminar;
               }
           }
           this.autos.remove(eliminado);
       }
   }
}
