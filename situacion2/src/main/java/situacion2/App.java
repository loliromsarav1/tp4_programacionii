/*
 * This Java source file was generated by the Gradle 'init' task.
 */
package situacion2;

import Interfaz.Ventana;
import Excepciones.ComponenteErroneoException;
import Excepciones.ListaAutoVaciaException;
import Excepciones.ListaVehiculoVaciaException;

public class App {
   

    public static void main(String[] args) throws ComponenteErroneoException, ListaAutoVaciaException, ListaVehiculoVaciaException {
       (new Ventana()).setVisible(true);
    }
}
