package situacion2;

public abstract class Auto extends VehiculoPasajeros implements InterfaceAlquilerVentaVehiculo {

    protected int precioBaseVenta;

    public Auto(String marca, String patente, int precioAlquilerDiario, int cantidadPlazas, int precioBaseVenta) {
        super(marca, patente, precioAlquilerDiario, cantidadPlazas);
        this.precioBaseVenta = precioBaseVenta;
    }

    public int getPrecioBaseVenta() {
        return precioBaseVenta;
    }


    
}
