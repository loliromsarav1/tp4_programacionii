package situacion2;

public abstract class VehiculoCarga extends Vehiculo{

    protected final float PRECIO_POR_KILOMETRAJE;

    public VehiculoCarga(int kilometraje,String marca, String patente, int precioAlquilerDiario, float pRECIO_POR_KILOMETRAJE) {
        super(marca, patente, precioAlquilerDiario);
        if(kilometraje >= 50)
        {
            PRECIO_POR_KILOMETRAJE = kilometraje*50;
        }
        else{
            PRECIO_POR_KILOMETRAJE = 300;
        }
    }

    public float getPRECIO_POR_KILOMETRAJE() {
        return PRECIO_POR_KILOMETRAJE;
    }

    
}
