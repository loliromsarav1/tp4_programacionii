package situacion1;
import Excepciones.CalificacionNegativaException;
import Excepciones.DatoErroneoEstudianteException;
import Excepciones.ListaVaciaException;
import Excepciones.PromedioImposibleException;
import org.junit.Test;
import static org.junit.Assert.*;


public class EstudianteTest {
    @Test
    public void constructor() throws CalificacionNegativaException, DatoErroneoEstudianteException, PromedioImposibleException, ListaVaciaException
    {
        
        Estudiante estudiante = new Estudiante("Lila", "Ibáñez", 15, 4598623, "Tinogasta");
        assertEquals("Lila Ibáñez",estudiante.getNombre());
        assertEquals((int)15,estudiante.getEdad());
        assertEquals((int)4598623,estudiante.getDocumento());
        assertEquals("Tinogasta",estudiante.getCiudad());

        estudiante.anadirCalificacion(7);
        estudiante.anadirCalificacion(8);
        estudiante.anadirCalificacion(6);

        assertEquals((int)7,estudiante.obtenerPromedioEstudiante());
    }
    
}
