package Excepciones;


public class DatoErroneoEstudianteException extends Exception {


    public DatoErroneoEstudianteException(String mensaje) {
        super(mensaje);
    }
}
