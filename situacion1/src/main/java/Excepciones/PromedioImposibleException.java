package Excepciones;

public class PromedioImposibleException extends Exception{

    public PromedioImposibleException(String mensaje) {
        super(mensaje);
    }
}
