package Excepciones;

public class ListaVaciaException  extends Exception {

    
    public ListaVaciaException(String mensaje) {
        super(mensaje);
    }
}
