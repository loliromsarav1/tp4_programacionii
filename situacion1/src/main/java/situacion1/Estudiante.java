package situacion1;

import java.util.ArrayList;
import Excepciones.CalificacionNegativaException;
import Excepciones.DatoErroneoEstudianteException;
import Excepciones.PromedioImposibleException;

public class Estudiante {

    private String nombre;
    private String apellido;
    private int edad;
    private long documento;
    private String ciudad;
    private ArrayList<Integer> calificacion; 

    public Estudiante(String nombre, String  apellido, int edad, long documento, String ciudad) throws DatoErroneoEstudianteException {
        if(edad > 0 && documento > 0){
            this.nombre = nombre;
            this.apellido = apellido;
            this.edad = edad;
            this.documento = documento;
            this.ciudad = ciudad;
            calificacion = new ArrayList<Integer>();
        }
        else{
            throw new DatoErroneoEstudianteException("La edad o documento ingresados son inv�lidos");
        }
    }
    
    //métodos setters y getters

    public String getNombre() {
        return nombre;
    }


    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido()
    {
        return apellido;
    }


    public void setApellido(String apellido)
    {
        this.apellido = apellido;
    }
    
    public int getEdad() {
        return edad;
    }



    public void setEdad(int edad) {
        this.edad = edad;
    }



    public long getDocumento() {
        return documento;
    }



    public void setDocumento(long documento) {
        this.documento = documento;
    }



    public String getCiudad() {
        return ciudad;
    }



    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }



    public ArrayList<Integer> getCalificacion() {
        return calificacion;
    }



    public void setCalificacion(ArrayList<Integer> calificacion) {
        this.calificacion = calificacion;
    }



    //metodo para agregar mas calificaciones a un estudiante
    public void anadirCalificacion(int calificacion) throws CalificacionNegativaException
    {
        if(calificacion >=0 && calificacion < 11)
        {
             this.calificacion.add(calificacion);
        }
        else
        {
            throw new CalificacionNegativaException("Se ha intentado agregar una calificacion negativa.");
        }

    }

    //metodo para obtener el promedio de un estudiante
    public int obtenerPromedioEstudiante() throws PromedioImposibleException
    {
        int promedio = 0;
        if(!this.calificacion.isEmpty()){
            for(int i : this.calificacion)
            {
                  promedio += i;
             }
        }
        if(this.calificacion.size() > 0){
             promedio = promedio/this.calificacion.size();
        }
        else{
            throw new PromedioImposibleException("El promedio no se puede calcular. Debe existir al menos una o m�s calificaciones");
        }
        return promedio;
    }



    

    
}
