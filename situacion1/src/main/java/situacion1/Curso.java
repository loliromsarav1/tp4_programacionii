package situacion1;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import Excepciones.PromedioImposibleException;
import Excepciones.ListaVaciaException;

import  java.util.Collections;
import java.util.Comparator;

public class Curso {
    
    private String nombreCurso;
    private int codigo;
    private ArrayList<Estudiante> estudiantes;


    //m茅todo constructor

    public Curso(String nombreCurso, int codigo) {
        this.nombreCurso = nombreCurso;
        this.codigo = codigo;
        estudiantes = new ArrayList<>();
    }

    //m茅todos setters y getters

    public String getNombreCurso() {
        return nombreCurso;
    }


    public void setNombreCurso(String nombreCurso) {
        this.nombreCurso = nombreCurso;
    }


    public int getCodigo() {
        return codigo;
    }


    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    
    /*#resetearNotas
    "Pone en cero las calificaciones de todos los estudiantes. "*/

    public boolean resetearNotas()
    {
        for(Estudiante estudiante : estudiantes)
        {
            estudiante.getCalificacion().clear();
            estudiante.getCalificacion().add(0);
        }
        return true;
    }

    /*#agregarEstudiante: unEstudiante
    "Agrega unEstudiante al curso"*/

    public void agregarEstudiante(Estudiante estudiante)
    {
        this.estudiantes.add(estudiante);
    }


    /*#cantidadDeEstudiantesInscriptos
    "Retorna la cantidad de alumnos que se inscribieron al curso"*/

    public int cantidadDeEstudiantesInscriptos()
    {
        return this.estudiantes.size();
    }

    /*#estudiantes
    "Retorna la colecci贸n de estudiantes del curso"*/

    public ArrayList<Estudiante> estudiantes() throws ListaVaciaException
    {
        if(!this.estudiantes.isEmpty()){
            Collections.sort(this.estudiantes,new Comparator<Estudiante>() {
                  @Override
                  public  int compare(Estudiante objeto1, Estudiante objeto2)
                  {
                      return objeto1.getApellido().compareTo(objeto2.getApellido());
                  }
                
            });
        }
        else{
            throw new ListaVaciaException("La lista se encuentra vac韆");
        }
        return this.estudiantes;
    }

    /*#estudiantesAprobados
    "Retorna una colecci贸n con todos los estudiantes que aprobaron el curso (calificaci贸n
    superior o igual a 4)"*/

    public ArrayList<Estudiante> estudiantesAprobados() throws PromedioImposibleException, ListaVaciaException
    {

        if(!this.estudiantes.isEmpty()){
                  ArrayList<Estudiante> aprobados = new ArrayList<Estudiante>();

                  for(Estudiante i : this.estudiantes)
                  {
                           if(i.obtenerPromedioEstudiante() >= 4)
                           {
                                     aprobados.add(i);
                            }
                  }
        
                  Collections.sort(aprobados,new Comparator<Estudiante>()
                  {
                           @Override
                           public int compare(Estudiante o1, Estudiante o2)
                           {
                                    int comparacion;
                                    try
                                    {
                                             comparacion = Integer.compare(o1.obtenerPromedioEstudiante(), o2.obtenerPromedioEstudiante());
                                             return comparacion;
                                    }
                                    catch(PromedioImposibleException e)
                                    {
                                              System.out.println("Error al intentar comparar promedio. Promedio incalculable");
                                             return 0;
                                     }
                           }
                  });
                  return aprobados;
        }
        else
        {
            throw new ListaVaciaException("La lista se encuentra vac韆");
        }
    }

    /*#existeEstudiante: unEstudiante
    "Indica si unEstudiante se encuentra inscripto en el curso"*/

    public boolean existeEstudiante()
    {
        if(estudiantes.isEmpty())
        {
            return false;
        }
        else{
            return true;
        }

    }

    /*#existeEstudianteConNotaDiez
    "Determina si alg煤n alumno obtuvo la calificaci贸n 10"*/


    public boolean existeEstudianteConNotaDiez() throws PromedioImposibleException
    {
        boolean existe = false;

        for (Estudiante estudiante : this.estudiantes)
        {
            if(estudiante.obtenerPromedioEstudiante() == 10)
            {
                existe = true;
            }
        }
        return existe;
    }

    /*#existeEstudianteLlamado: aString
    "Indica si el estudiante llamado aString se encuentra inscripto en el curso"*/

    public boolean existeEstudianteLlamado(String nombre)
    {
        boolean existe = false;

        for(Estudiante estudiante : this.estudiantes)
        {
            if(nombre == estudiante.getNombre())
            {
                existe = true;
            }
        }
        return existe;
    }

     public boolean existeEstudianteLlamado(String nombre, String apellido)
    {
        boolean existe=false;
        for (Estudiante estudiante : this.estudiantes)
        {
            if (estudiante.getNombre().equals(nombre) && estudiante.getApellido().equals(apellido))
            {
                existe = true;
            }
        }
        return existe;
    }
     
     public Estudiante buscarEstudiante(String nombre, String apellido)
    {
        if (existeEstudianteLlamado(nombre,apellido))
        {
            for (Estudiante estudiante : this.estudiantes)
            {
              if (estudiante.getNombre().equals(nombre) && estudiante.getApellido().equals(apellido))
              {
                  return estudiante;
              }
            }
        }
        return null;
    }
     
      public boolean eliminarEstudiante(String nombre, String apellido)
    {
        if (this.existeEstudianteLlamado(nombre,apellido))
        {
            this.estudiantes.remove(this.buscarEstudiante(nombre, apellido));
            return true;
        }
        else
        {
            return false;
        }
    }
    /*#porcentajeDeAprobados
    "Retorna en porcentaje de estudiantes aprobados"*/


    public int porcentajeDeAprobados() throws PromedioImposibleException
    {
        int porcentajeAprobados=0; 

        for(Estudiante estudiante : this.estudiantes)
        {
            if(estudiante.obtenerPromedioEstudiante() >= 4)
            {
                porcentajeAprobados += 1;
            }
        }
        porcentajeAprobados = (porcentajeAprobados*100)/estudiantes.size();
        
        return porcentajeAprobados;
    }


    /*#promedioDeCalificaciones
    "Calcula el promedio de las calificaciones obtenidas por los alumnos"*/


    public int promedioDeCalificaciones() throws PromedioImposibleException
    {
        int promedioEstudiantes=0;

        for(Estudiante estudiante : this.estudiantes)
        {
            promedioEstudiantes += estudiante.obtenerPromedioEstudiante();
        }
        promedioEstudiantes = promedioEstudiantes/estudiantes.size();
        
        return promedioEstudiantes;
    }


    /*#estudiantes Del Interior Provinial
    "Retorna una colecci贸n con todos los estudiantes que no nacieron en la capital "*/


    public ArrayList<Estudiante> estudiantesDelInteriorProvincial() throws ListaVaciaException
    {
        if(!this.estudiantes.isEmpty()) {
            ArrayList<Estudiante> estudiantesInterior = new ArrayList<Estudiante>();

             for(Estudiante estudiante : this.estudiantes)
             {
                  if(estudiante.getCiudad() != "Capital")
                  {
                        estudiantesInterior.add(estudiante);
                  }
             }
             Collections.sort(estudiantesInterior, new Comparator<Estudiante>()
             {
                 @Override
                 public int compare(Estudiante objeto1,Estudiante objeto2)
                 {
                     int comparacion;
                     comparacion = objeto1.getApellido().compareTo(objeto2.getApellido());
                     if(comparacion == 0)
                     {
                         comparacion = objeto1.getCiudad().compareTo(objeto2.getCiudad());
                     }
                     return comparacion;
                     
                 }
             });
             return estudiantesInterior;
        }
        else
        {
            throw new ListaVaciaException("Lista vac韆");
        }
    }

    /*#ciudadesExceptoCapital
    "Retorna una colecci贸n, sin repeticiones, conteniendo los nombres de todas las
    ciudades do nde nacieron los alumnos inscriptos al curso"*/

    public ArrayList<String> ciudadesExceptoCapital() throws  ListaVaciaException
    {
        
              if(!this.estudiantes.isEmpty())
               {
                    ArrayList<String> ciudadesSinRepetir = new ArrayList<String>();

                    for(Estudiante estudiante : this.estudiantes)
                    {
                        if(estudiante.getCiudad() != "Capital")
                        {
                              ciudadesSinRepetir.add(estudiante.getCiudad());
                         }
                    }
                    Set<String> noRepetidas = new HashSet<>(ciudadesSinRepetir);
                    ciudadesSinRepetir.clear();
                    ciudadesSinRepetir.addAll(noRepetidas);
                    
                    Collections.sort(ciudadesSinRepetir, new Comparator<String>() {
                         @Override
                         public int compare(String objeto1, String objeto2)
                         {
                               return objeto1.compareTo(objeto2);
                         }
                   });
                   return ciudadesSinRepetir;
               }
              else{
                  throw new ListaVaciaException("Lista vac韆");
              }   
        
    }


    /*#unDesastre
    "Retorna verdadero si todos los estudiantes desaprobaron el curso"*/  

    public boolean unDesastre() throws  PromedioImposibleException
    {
        boolean desaprobados = true;

        for(Estudiante estudiante : this.estudiantes)
        {
            if(estudiante.obtenerPromedioEstudiante() < 4)
            {
                desaprobados = false;
            }
        }
        return desaprobados;
    }
    

}
