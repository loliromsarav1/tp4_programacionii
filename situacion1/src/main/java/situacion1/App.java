/*
 This Java source file was generated by the Gradle 'init' task.
 */


package situacion1;
import Excepciones.CalificacionNegativaException;
import Excepciones.DatoErroneoEstudianteException;
import Excepciones.PromedioImposibleException;
import Excepciones.ListaVaciaException;
import Interfaz.Ventana;

public class App {
   
    public static void main(String[] args) throws CalificacionNegativaException, DatoErroneoEstudianteException, PromedioImposibleException, ListaVaciaException {
        
        Ventana ventana = new Ventana();
        ventana.setVisible(true);
    }
}
